@extends('layouts.master')
@section('title', 'Show')

@section('content')

      <div class="card-header">
        <h3 class="card-title">Show</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <a class="btn btn-primary" href="/pertanyaan/create">Back</a>
        <table class="table table-bordered">
          <thead>                  
            <tr>
              <th style="width: 10px">#</th>
              <th>Judul</th>
              <th>Isi</th>
            </tr>
          </thead>
          <tbody>
           
              <tr>
                <td> Question </td>
                <td> {{ $tanya->judul}} </td>
                <td> {{ $tanya->isi}} </td>
              </tr>
           
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      
    </div>
@endsection
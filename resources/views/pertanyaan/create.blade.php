@extends('layouts.master')
@section('title', 'Pertanyaan/Create')

@section('content')
	<div class="card card-primary">
	  <div class="card-header">
	    <h3 class="card-title">Create New Question</h3>
	  </div>
	  <!-- /.card-header -->
	  <!-- form start -->
	  <form role="form" action="/pertanyaan" method="POST">
	  	@csrf
	    <div class="card-body">
	      <div class="form-group">
	        <label for="judul">Judul</label>
	        <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukan Judul">
	      	@error('judul')
	      		<div class="alert alert-danger">{{ $message }}</div>
	      	@enderror
	      </div>
	      <div class="form-group">
	        <label for="isi">Isi</label>
	        <input type="text" class="form-control" id="isi" name="isi" placeholder="Masukan isi">
	      	@error('isi')
	      		<div class="alert alert-danger">{{ $message }}</div>
	      	@enderror
	      </div>
	    </div>
	    <!-- /.card-body -->

	    <div class="card-footer">
	      <button type="submit" class="btn btn-primary">Create</button>
	      <a class="btn btn-warning" href="/pertanyaan">Show</a>
	    </div>
	  </form>
	</div>
@endsection
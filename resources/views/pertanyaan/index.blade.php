@extends('layouts.master')
@section('title', 'Pertanyaan')

@section('content')
	<div class="card">
      <div class="card-header">
        <h3 class="card-title">Show Question</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <a class="btn btn-primary" href="{{route('pertanyaan.create')}}">Create New Question</a>
        <table class="table table-bordered">
          <thead>                  
            <tr>
              <th style="width: 10px">#</th>
              <th>Judul</th>
              <th>Isi</th>
              <th style="width: 40px">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($pertanyaan as $key => $pertanyaan)
              <tr>
                <td> {{ $key +1}} </td>
                <td> {{ $pertanyaan->judul}} </td>
                <td> {{ $pertanyaan->isi}} </td>
                <td style="display: flex;">
                  <a href="{{route('pertanyaan.show', ['pertanyaan' => $pertanyaan->id])}}" class="btn btn-info btn-sm">Show</a>
                  <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <form action="/pertanyaan/{{$pertanyaan->id}}" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      
    </div>
@endsection
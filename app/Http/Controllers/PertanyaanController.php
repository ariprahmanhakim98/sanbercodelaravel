<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create(){
    	return view('pertanyaan.create');
    }


    public function store(Request $request){
    	// dd($request->all());
    	$request->validate([
    		'judul' => 'required|unique:pertanyaan',
    		'isi' => 'required'
    	]);

        //dengan query builder
    	// $query = DB::table('pertanyaan')->insert([
    	// 	"judul" => $request["judul"],
    	// 	"isi" => $request["isi"]
    	// ]);

        //dengan model
        // $tanya = new Pertanyaan;
        // $tanya->judul = $request["judul"];
        // $tanya->isi = $request["isi"];
        // $tanya->save();

        //dengan Mass Asigment
        $tanya = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);


    	return redirect('/pertanyaan/create');
    }

    public function index(){
    	//dengan query builder
        // $pertanyaan = DB::table('pertanyaan')->get();

        //dengan model
        $pertanyaan = Pertanyaan::all();

    	return view('pertanyaan.index',compact('pertanyaan'));
    }

    public function show($id){
    	// $tanya = DB::table('pertanyaan')->where('id', $id)->first();

        //dengan model
        $tanya = Pertanyaan::find($id);
    	return view('pertanyaan.show',compact('tanya'));
    }

    public function edit($id){
    	// $tanya = DB::table('pertanyaan')->where('id', $id)->first();

        //dengan model
        $tanya = Pertanyaan::find($id);
    	return view('pertanyaan.edit', compact('tanya'));
    }

    public function update($id, Request $request){

    	// $query = DB::table('pertanyaan')
    	// 			->where('id', $id)
    	// 			->update([
    	// 				'judul' => $request['judul'],
    	// 				'isi' => $request['isi']
    	// 			]);

        //dengan model
        $update = Pertanyaan::where('id', $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

    	return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan!');
    }

    public function destroy($id){
    	// $query = DB::table('pertanyaan')->where('id', $id)->delete();

        //dengan model
        Pertanyaan::destroy($id);
    	return redirect('/pertanyaan')->with('success', 'Berhasil hapus pertanyaan!');
    }
}
